<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/login', [UserController::class, 'loginPage'])->name('login');
Route::get('/register', [UserController::class, 'registerPage'])->name('register');

Route::post('/do-register', [UserController::class, 'register'])->name('do-register');
Route::post('/do-login', [UserController::class, 'login'])->name('do-login');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard');
});
