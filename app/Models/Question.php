<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    public function answers()
    {
        return $this->hasMany(Answer::class)->attach('id', [
            'title' => 'd,dgdfgdfn'
        ]);
    }

    public function getAll()
    {
        return self::get();
    }
}
