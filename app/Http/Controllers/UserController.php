<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function loginPage()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }

        return view('user.login');
    }

    public function registerPage()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }

        return view('user.register');
    }
    
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
    
        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        }

        $credentials = $request->only("email", "password");
        if (Auth::attempt($credentials)) {
            return redirect()->route('dashboard')->with("success", "User created successfully.");
        }

        return redirect()->back()->withInput()->with(['error' => 'Login details are not valid!.']);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'agree-term' => 'accepted',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        $credentials = $request->only("email", "password");
        if (Auth::attempt($credentials)) {
            return redirect()->route('/dashboard')->with("success", "User created successfully.");
        }

        return redirect()->route("register")->with("message", "register details are not valid!");
    }

    public function index()
    {
        return view('user.home');
    }
}
